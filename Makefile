start:
	docker-compose build
	docker-compose up -d

stop:
	docker-compose down

deploy:
	git push origin master
	git push gandi master
	ssh 63639@git.sd6.gpaas.net deploy datacenter.anthony-dessalles.com.git

exec:
	docker-compose exec php bash