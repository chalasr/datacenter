<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170323214242 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity CHANGE end end DATETIME DEFAULT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D6495E237E06 ON user');
        $this->addSql('ALTER TABLE user ADD surname VARCHAR(45) NOT NULL, ADD username VARCHAR(45) NOT NULL, CHANGE name name VARCHAR(45) NOT NULL, CHANGE password password VARCHAR(88) DEFAULT NULL, CHANGE salt salt VARCHAR(23) DEFAULT NULL, CHANGE role role VARCHAR(50) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE activity CHANGE end end DATETIME NOT NULL');
        $this->addSql('DROP INDEX UNIQ_8D93D649F85E0677 ON user');
        $this->addSql('ALTER TABLE user DROP surname, DROP username, CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci, CHANGE password password VARCHAR(88) NOT NULL COLLATE utf8_unicode_ci, CHANGE salt salt VARCHAR(23) NOT NULL COLLATE utf8_unicode_ci, CHANGE role role VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6495E237E06 ON user (name)');
    }
}
