<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180914175231 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE action_name (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE timeline (id INT AUTO_INCREMENT NOT NULL, start DATETIME NOT NULL, end DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE action (id INT AUTO_INCREMENT NOT NULL, action_name_id INT DEFAULT NULL, start DATETIME NOT NULL, end DATETIME DEFAULT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_47CC8C92ABB51437 (action_name_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_has_action (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, action_id INT DEFAULT NULL, description LONGTEXT DEFAULT NULL, INDEX IDX_CC895DE5A76ED395 (user_id), INDEX IDX_CC895DE59D32F035 (action_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE action ADD CONSTRAINT FK_47CC8C92ABB51437 FOREIGN KEY (action_name_id) REFERENCES action_name (id)');
        $this->addSql('ALTER TABLE user_has_action ADD CONSTRAINT FK_CC895DE5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_has_action ADD CONSTRAINT FK_CC895DE59D32F035 FOREIGN KEY (action_id) REFERENCES action (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE action DROP FOREIGN KEY FK_47CC8C92ABB51437');
        $this->addSql('ALTER TABLE user_has_action DROP FOREIGN KEY FK_CC895DE59D32F035');
        $this->addSql('DROP TABLE action_name');
        $this->addSql('DROP TABLE timeline');
        $this->addSql('DROP TABLE action');
        $this->addSql('DROP TABLE user_has_action');
    }
}
