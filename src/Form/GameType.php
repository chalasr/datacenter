<?php

namespace App\Form;

use App\Entity\Media;
use App\Entity\Type;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\Game;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('slug', TextType::class)
            ->add('medias', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_medias',
                'class' => Media::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a media'
            ])
            ->add('users', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_users',
                'class' => User::class,
                'primary_key' => 'id',
                'text_property' => 'username',
                'placeholder' => 'Select a user'
            ])
            ->add('types', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_types',
                'class' => Type::class,
                'primary_key' => 'id',
                'text_property' => 'slug',
                'placeholder' => 'Select a type'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Game::class,
        ));
    }
}