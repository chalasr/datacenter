<?php

namespace App\Form;

use App\Entity\Place;
use App\Entity\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Activity;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('start', DateTimeType::class)
            ->add('end', DateTimeType::class)
            ->add('description', TextareaType::class, [
            	'required'  => false,
        	])
            ->add('type', EntityType::class, [
            	'class' => Type::class,
            	'choice_label' => 'slug',
        	])
            ->add('place', EntityType::class, [
            	'class' => Place::class,
            	'choice_label' => 'name',
        	])
        	->add('score', ScoreType::class, [
            	'required'  => false,
        	])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Activity::class,
        ]);
    }
}