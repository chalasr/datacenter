<?php

namespace App\Form;

use App\Entity\Link;
use App\Entity\Login;
use App\Entity\Password;
use App\Entity\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Data;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;

class DataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextareaType::class, [
                'required' => false
            ])
            ->add('type', EntityType::class, [
            	'class' => Type::class,
            	'choice_label' => 'slug'
        	])
            ->add('login', EntityType::class, [
                'class' => Login::class,
                'choice_label' => 'name',
                'required' => false
            ])
            ->add('password', EntityType::class, [
            	'class' => Password::class,
            	'choice_label' => 'hint',
                'required' => false
        	])
            ->add('links', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_links',
                'class' => Link::class,
                'primary_key' => 'id',
                'text_property' => 'url',
                'placeholder' => 'Select a link'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Data::class,
        ]);
    }
}