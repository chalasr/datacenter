<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use App\Entity\Score;

class ScoreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('homeAwayDifference', NumberType::class, [
                'required'  => false,
            ])
            ->add('homePoints', NumberType::class, [
                'required'  => false,
            ])
            ->add('awayPoints', NumberType::class, [
                'required'  => false,
            ])
            ->add('homePlayers', NumberType::class, [
                'required'  => false,
            ])
            ->add('awayPlayers', NumberType::class, [
                'required'  => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Score::class,
        ));
    }
}