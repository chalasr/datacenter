<?php

namespace App\Form;

use App\Entity\File;
use App\Entity\Game;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Tetranz\Select2EntityBundle\Form\Type\Select2EntityType;
use App\Entity\Media;

class MediaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('date', DateTimeType::class)
            ->add('confrontation', TextType::class)
            ->add('difficulty', TextType::class)
            ->add('description', TextareaType::class, [
            	'required'  => false,
        	])
            ->add('file', EntityType::class, [
            	'class' => File::class,
            	'choice_label' => 'filename',
        	])
            ->add('games', Select2EntityType::class, [
                'multiple' => true,
                'remote_route' => 'ajax_games',
                'class' => Game::class,
                'primary_key' => 'id',
                'text_property' => 'name',
                'placeholder' => 'Select a game'
            ])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Media::class,
        ));
    }
}