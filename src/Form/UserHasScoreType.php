<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Score;
use App\Entity\User;
use App\Repository\ScoreRepository;

class UserHasScoreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isHome', CheckboxType::class, [
                'data' => true,
            ])
            ->add('points', NumberType::class)
            ->add('assists', NumberType::class, [
                'required' => false
            ])
            ->add('saves', NumberType::class, [
                'required' => false
            ])
            ->add('score', EntityType::class, [
                'class' => Score::class,
                'choice_label' => function(Score $score, $key, $index) {
                    return $score->getActivity()->getName() . ' ' . $score->getActivity()->getStart()->format('d/m/Y H:i');
                },
            ])
            ->add('user', EntityType::class, [
            	'class' => User::class,
            	'choice_label' => 'name',
        	])
            ->add('save', SubmitType::class)
        ;
    }
}