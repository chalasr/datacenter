<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserHasScore
 *
 * @ORM\Table(name="user_has_score")
 * @ORM\Entity(repositoryClass="App\Repository\UserHasScoreRepository")
 */
class UserHasScore
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_home", type="boolean")
     */
    private $isHome;

    /**
     * @var float
     *
     * @ORM\Column(name="points", type="float")
     */
    private $points;

    /**
     * @var float
     *
     * @ORM\Column(name="assists", type="float", nullable=true)
     */
    private $assists;

    /**
     * @var float
     *
     * @ORM\Column(name="saves", type="float", nullable=true)
     */
    private $saves;

    /**
     * @ORM\ManyToOne(targetEntity="Score", inversedBy="scoreUsers")
     * @ORM\JoinColumn(name="score_id", referencedColumnName="id")
     */
    private $score;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userScores")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isHome
     *
     * @param boolean $isHome
     *
     * @return UserHasScore
     */
    public function setIsHome($isHome)
    {
        $this->isHome = $isHome;

        return $this;
    }

    /**
     * Get isHome
     *
     * @return bool
     */
    public function getIsHome()
    {
        return $this->isHome;
    }

    /**
     * Set points
     *
     * @param float $points
     *
     * @return UserHasScore
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return float
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set assists
     *
     * @param float $assists
     *
     * @return UserHasScore
     */
    public function setAssists($assists)
    {
        $this->assists = $assists;

        return $this;
    }

    /**
     * Get assists
     *
     * @return float
     */
    public function getAssists()
    {
        return $this->assists;
    }

    /**
     * Set saves
     *
     * @param float $saves
     *
     * @return UserHasScore
     */
    public function setSaves($saves)
    {
        $this->saves = $saves;

        return $this;
    }

    /**
     * Get saves
     *
     * @return float
     */
    public function getSaves()
    {
        return $this->saves;
    }

    /**
     * Set score
     *
     * @param \App\Entity\Score $score
     *
     * @return UserHasScore
     */
    public function setScore(\App\Entity\Score $score = null)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return \App\Entity\Score
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return UserHasUser
     */
    public function setUser(\App\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
