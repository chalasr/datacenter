<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Password
 *
 * @ORM\Table(name="password")
 * @ORM\Entity(repositoryClass="App\Repository\PasswordRepository")
 */
class Password
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hint", type="string", length=255)
     */
    private $hint;

    /**
     * @ORM\OneToMany(targetEntity="Data", mappedBy="password")
     */
    private $datas;

    public function __construct() {
        $this->datas = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hint
     *
     * @param string $hint
     *
     * @return Password
     */
    public function setHint($hint)
    {
        $this->hint = $hint;

        return $this;
    }

    /**
     * Get hint
     *
     * @return string
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * Add data
     *
     * @param \App\Entity\Data $data
     *
     * @return Password
     */
    public function addData(\App\Entity\Data $data)
    {
        $this->datas[] = $data;

        return $this;
    }

    /**
     * Remove data
     *
     * @param \App\Entity\Data $data
     */
    public function removeData(\App\Entity\Data $data)
    {
        $this->datas->removeElement($data);
    }

    /**
     * Get datas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDatas()
    {
        return $this->datas;
    }
}
