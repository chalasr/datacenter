<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity(repositoryClass="App\Repository\TypeRepository")
 */
class Type
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="font_awesome", type="string", length=45, nullable=true)
     */
    private $fontAwesome;

    /**
     * @ORM\OneToMany(targetEntity="Data", mappedBy="type")
     */
    private $datas;

    /**
     * @ORM\OneToMany(targetEntity="File", mappedBy="type")
     */
    private $files;

    /**
     * @ORM\OneToMany(targetEntity="Activity", mappedBy="type")
     */
    private $activities;

    /**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="types")
     */
    private $games;

    public function __construct() {
        $this->datas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Type
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add data
     *
     * @param \App\Entity\Data $data
     *
     * @return Type
     */
    public function addData(\App\Entity\Data $data)
    {
        $this->datas[] = $data;

        return $this;
    }

    /**
     * Remove data
     *
     * @param \App\Entity\Data $data
     */
    public function removeData(\App\Entity\Data $data)
    {
        $this->datas->removeElement($data);
    }

    /**
     * Get datas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDatas()
    {
        return $this->datas;
    }

    /**
     * Set fontAwesome
     *
     * @param string $fontAwesome
     *
     * @return Type
     */
    public function setFontAwesome($fontAwesome)
    {
        $this->fontAwesome = $fontAwesome;

        return $this;
    }

    /**
     * Get fontAwesome
     *
     * @return string
     */
    public function getFontAwesome()
    {
        return $this->fontAwesome;
    }

    /**
     * Add data
     *
     * @param \App\Entity\Activity $activity
     *
     * @return Type
     */
    public function addActivity(\App\Entity\Activity $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param \App\Entity\Activity $activity
     */
    public function removeActivity(\App\Entity\Activity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Add file
     *
     * @param \App\Entity\File $file
     *
     * @return Type
     */
    public function addFile(\App\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \App\Entity\File $file
     */
    public function removeFile(\App\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add game
     *
     * @param \App\Entity\Game $game
     *
     * @return Type
     */
    public function addGame(\App\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \App\Entity\Game $game
     */
    public function removeGame(\App\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }
}
