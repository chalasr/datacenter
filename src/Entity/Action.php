<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Table(name="action")
 * @ORM\Entity(repositoryClass="App\Repository\ActionRepository")
 */
class Action
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="ActionName", inversedBy="actions")
     * @ORM\JoinColumn(name="action_name_id", referencedColumnName="id")
     */
    private $actionName;

    /**
     * @ORM\OneToMany(targetEntity="UserHasAction", mappedBy="action")
     */
    private $actionUsers;

    public function __construct() {
        $this->actionUsers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Action
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Action
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Action
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set actionName
     *
     * @param \App\Entity\ActionName $actionName
     *
     * @return Action
     */
    public function setActionName(\App\Entity\ActionName $actionName = null)
    {
        $this->actionName = $actionName;

        return $this;
    }

    /**
     * Get actionName
     *
     * @return \App\Entity\ActionName
     */
    public function getActionName()
    {
        return $this->actionName;
    }

    /**
     * Add actionUser
     *
     * @param \App\Entity\UserHasAction $actionUser
     *
     * @return Action
     */
    public function addActionUser(\App\Entity\UserHasAction $actionUser)
    {
        $this->actionUsers[] = $actionUser;

        return $this;
    }

    /**
     * Remove actionUser
     *
     * @param \App\Entity\UserHasAction $actionUser
     */
    public function removeActionUser(\App\Entity\UserHasAction $actionUser)
    {
        $this->actionUsers->removeElement($actionUser);
    }

    /**
     * Get actionUsers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActionUsers()
    {
        return $this->actionUsers;
    }
}
