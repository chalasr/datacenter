<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Score
 *
 * @ORM\Table(name="score")
 * @ORM\Entity(repositoryClass="App\Repository\ScoreRepository")
 */
class Score
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="home_away_difference", type="float")
     */
    private $homeAwayDifference;

    /**
     * @var float
     *
     * @ORM\Column(name="home_points", type="float", nullable=true)
     */
    private $homePoints;

    /**
     * @var float
     *
     * @ORM\Column(name="away_points", type="float", nullable=true)
     */
    private $awayPoints;

    /**
     * @var int
     *
     * @ORM\Column(name="home_players", type="integer", nullable=true)
     */
    private $homePlayers;

    /**
     * @var int
     *
     * @ORM\Column(name="away_players", type="integer", nullable=true)
     */
    private $awayPlayers;

    /**
     * One Score has One Activity.
     * @ORM\OneToOne(targetEntity="Activity", inversedBy="score", cascade={"persist"})
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;

    /**
     * @ORM\OneToMany(targetEntity="UserHasScore", mappedBy="score")
     */
    private $scoreUsers;

    public function __construct() {
        $this->scoreUsers = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set homeAwayDifference
     *
     * @param float $homeAwayDifference
     *
     * @return Score
     */
    public function setHomeAwayDifference($homeAwayDifference)
    {
        $this->homeAwayDifference = $homeAwayDifference;

        return $this;
    }

    /**
     * Get homeAwayDifference
     *
     * @return float
     */
    public function getHomeAwayDifference()
    {
        return $this->homeAwayDifference;
    }

    /**
     * Set homePoints
     *
     * @param float $homePoints
     *
     * @return Score
     */
    public function setHomePoints($homePoints)
    {
        $this->homePoints = $homePoints;

        return $this;
    }

    /**
     * Get homePoints
     *
     * @return float
     */
    public function getHomePoints()
    {
        return $this->homePoints;
    }

    /**
     * Set awayPoints
     *
     * @param float $awayPoints
     *
     * @return Score
     */
    public function setAwayPoints($awayPoints)
    {
        $this->awayPoints = $awayPoints;

        return $this;
    }

    /**
     * Get awayPoints
     *
     * @return float
     */
    public function getAwayPoints()
    {
        return $this->awayPoints;
    }

    /**
     * Set homePlayers
     *
     * @param integer $homePlayers
     *
     * @return Score
     */
    public function setHomePlayers($homePlayers)
    {
        $this->homePlayers = $homePlayers;

        return $this;
    }

    /**
     * Get homePlayers
     *
     * @return int
     */
    public function getHomePlayers()
    {
        return $this->homePlayers;
    }

    /**
     * Set awayPlayers
     *
     * @param integer $awayPlayers
     *
     * @return Score
     */
    public function setAwayPlayers($awayPlayers)
    {
        $this->awayPlayers = $awayPlayers;

        return $this;
    }

    /**
     * Get awayPlayers
     *
     * @return int
     */
    public function getAwayPlayers()
    {
        return $this->awayPlayers;
    }

    /**
     * Add userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     *
     * @return Place
     */
    public function addScoreUsers(\App\Entity\UserHasScore $userScore)
    {
        $this->scoreUsers[] = $userScore;

        return $this;
    }

    /**
     * Remove userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     */
    public function removeScoreUsers(\App\Entity\UserHasScore $userScore)
    {
        $this->scoreUsers->removeElement($userScore);
    }

    /**
     * Get userScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScoreUsers()
    {
        return $this->scoreUsers;
    }

    /**
     * Set activity
     *
     * @param \App\Entity\Activity $activity
     *
     * @return Score
     */
    public function setActivity(\App\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \App\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Add scoreUser
     *
     * @param \App\Entity\UserHasScore $scoreUser
     *
     * @return Score
     */
    public function addScoreUser(\App\Entity\UserHasScore $scoreUser)
    {
        $this->scoreUsers[] = $scoreUser;

        return $this;
    }

    /**
     * Remove scoreUser
     *
     * @param \App\Entity\UserHasScore $scoreUser
     */
    public function removeScoreUser(\App\Entity\UserHasScore $scoreUser)
    {
        $this->scoreUsers->removeElement($scoreUser);
    }
}
