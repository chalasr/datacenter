<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Data
 *
 * @ORM\Table(name="data")
 * @ORM\Entity(repositoryClass="App\Repository\DataRepository")
 */
class Data
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Type", inversedBy="datas")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Login", inversedBy="datas")
     * @ORM\JoinColumn(name="login_id", referencedColumnName="id")
     */
    private $login;

    /**
     * @ORM\ManyToOne(targetEntity="Password", inversedBy="datas")
     * @ORM\JoinColumn(name="password_id", referencedColumnName="id")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="Link", inversedBy="datas")
     * @ORM\JoinTable(name="data_has_link")
     */
    private $links;

    public function __construct() {
        $this->links = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Data
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Data
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param \App\Entity\Type $type
     *
     * @return Data
     */
    public function setType(\App\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \App\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set login
     *
     * @param \App\Entity\Login $login
     *
     * @return Data
     */
    public function setLogin(\App\Entity\Login $login = null)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return \App\Entity\Login
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param \App\Entity\Password $password
     *
     * @return Data
     */
    public function setPassword(\App\Entity\Password $password = null)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return \App\Entity\Password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add link
     *
     * @param \App\Entity\Link $link
     *
     * @return Data
     */
    public function addLink(\App\Entity\Link $link)
    {
        $this->links[] = $link;

        return $this;
    }

    /**
     * Remove link
     *
     * @param \App\Entity\Link $link
     */
    public function removeLink(\App\Entity\Link $link)
    {
        $this->links->removeElement($link);
    }

    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks()
    {
        return $this->links;
    }
}
