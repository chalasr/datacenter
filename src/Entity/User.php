<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=45)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=45, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=88, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=23, nullable=true)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     */
    private $role;

    /**
     * @ORM\OneToMany(targetEntity="UserHasScore", mappedBy="user")
     */
    private $userScores;

    /**
     * @ORM\OneToMany(targetEntity="UserHasAction", mappedBy="user")
     */
    private $userActions;

    /**
     * @ORM\ManyToMany(targetEntity="Game", mappedBy="users")
     */
    private $games;

    public function __construct() {
        $this->userScores = new ArrayCollection();
        $this->games = new ArrayCollection();
        $this->userActions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Add userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     *
     * @return Place
     */
    public function addUserScore(\App\Entity\UserHasScore $userScore)
    {
        $this->userScores[] = $userScore;

        return $this;
    }

    /**
     * Remove userScore
     *
     * @param \App\Entity\UserHasScore $userScore
     */
    public function removeUserScore(\App\Entity\UserHasScore $userScore)
    {
        $this->userScores->removeElement($userScore);
    }

    /**
     * Get userScores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserScores()
    {
        return $this->userScores;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Add game
     *
     * @param \App\Entity\Game $game
     *
     * @return User
     */
    public function addGame(\App\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \App\Entity\Game $game
     */
    public function removeGame(\App\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    public function getRoles()
    {
        return [$this->getRole()];
    }

    public function eraseCredentials()
    {
    }


    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt
        ) = unserialize($serialized);
    }

    /**
     * Add userAction
     *
     * @param \App\Entity\UserHasAction $userAction
     *
     * @return User
     */
    public function addUserAction(\App\Entity\UserHasAction $userAction)
    {
        $this->userActions[] = $userAction;

        return $this;
    }

    /**
     * Remove userAction
     *
     * @param \App\Entity\UserHasAction $userAction
     */
    public function removeUserAction(\App\Entity\UserHasAction $userAction)
    {
        $this->userActions->removeElement($userAction);
    }

    /**
     * Get userActions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserActions()
    {
        return $this->userActions;
    }
}
