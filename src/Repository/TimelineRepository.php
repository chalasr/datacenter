<?php

namespace App\Repository;

use App\Entity\Timeline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TimelineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Timeline::class);
    }

    public function findAllOrderByStart($value)
    {
        $qb = $this->createQueryBuilder('t')
            ->orderBy('t.start', $value)
            ->getQuery();

        return $qb->execute();
    }
}