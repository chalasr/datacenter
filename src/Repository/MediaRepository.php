<?php

namespace App\Repository;

/**
 * MediaRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class MediaRepository extends \Doctrine\ORM\EntityRepository
{
	public function getMediasFromName($name)
	{
		return $this->getEntityManager()
            ->createQuery(
                'SELECT m
                FROM App\Entity\Media m
                WHERE m.name LIKE :name
                ORDER BY m.name ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
	}
}
