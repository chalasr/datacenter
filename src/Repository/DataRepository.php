<?php

namespace App\Repository;

use App\Entity\Type;

class DataRepository extends \Doctrine\ORM\EntityRepository
{
	public function findAll()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT d FROM App\Entity\Data d ORDER BY d.name ASC'
            )
            ->getResult();
    }

	public function findAllFromType(Type $type)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT d FROM App\Entity\Data d WHERE d.type = ' . $type->getId() . ' ORDER BY d.name ASC'
            )
            ->getResult();
    }
}
