<?php

namespace App\Repository;

use App\Entity\Activity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ActivityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Activity::class);
    }

	public function findAllOrderByStart($value)
    {
        $qb = $this->createQueryBuilder('a')
            ->orderBy('a.start', $value)
            ->getQuery();

        return $qb->execute();
    }

    public function findAllBetweenDates(\DateTime $from, \DateTime $to, $order)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.start BETWEEN :from AND :to')
            ->orderBy('a.start', $order)
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->getQuery();

        return $qb->execute();
    }
}
