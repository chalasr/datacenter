<?php

namespace App\Repository;

/**
 * UserRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
	public function getUsersFromUsername($name)
	{
		return $this->getEntityManager()
            ->createQuery(
                'SELECT u
                FROM App\Entity\User u 
                WHERE u.username LIKE :name 
                ORDER BY u.username ASC'
            )
            ->setParameter('name', '%'.$name.'%')
            ->getResult();
	}
}
