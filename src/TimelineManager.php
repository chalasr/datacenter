<?php

namespace App;

use App\Repository\ActionRepository;
use App\Repository\ActivityRepository;
use App\Repository\FileRepository;
use App\Repository\TimelineRepository;

class TimelineManager
{
    /** @var TimelineRepository */
    private $timelineRepository;

    /** @var ActivityRepository */
    private $activityRepository;

    /** @var ActionRepository */
    private $actionRepository;

    /** @var FileRepository */
    private $fileRepository;

    /**
     * TimelineManager constructor.
     * @param TimelineRepository $timelineRepository
     * @param ActivityRepository $activityRepository
     * @param ActionRepository $actionRepository
     * @param FileRepository $fileRepository
     */
    public function __construct(TimelineRepository $timelineRepository, ActivityRepository $activityRepository, ActionRepository $actionRepository, FileRepository $fileRepository)
    {
        $this->timelineRepository = $timelineRepository;
        $this->activityRepository = $activityRepository;
        $this->actionRepository = $actionRepository;
        $this->fileRepository = $fileRepository;
    }

    public function getCombinedTimeline(\DateTime $from, \DateTime $to) {
        $activities = $this->activityRepository->findAllBetweenDates($from, $to, 'DESC');
        $nbDays = $to->diff($from)->format("%a");

        for ($i=1; $i<=$nbDays; $i++) {
            $currentDate = $to->modify('-1 day');
            $combinedTimeline[$currentDate->format('Y-m-d')] = null;
        }

        foreach ($activities as $activity) {
            $activityDay = $activity->getStart()->format('Y-m-d');
            if (isset($combinedTimeline[$activityDay]['activities'])) {
                array_push($combinedTimeline[$activityDay]['activities'], $activity);
            } else {
                $combinedTimeline[$activityDay]['activities'] = [$activity];
            }
        }

        return $combinedTimeline;
    }
}