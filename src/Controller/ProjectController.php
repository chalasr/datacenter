<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Project;
use App\Form\ProjectType;

class ProjectController extends Controller
{
    /**
     * @Route("/projects", name="projects")
     */
    public function projectsAction()
    {
        return $this->render('project/projects.html.twig', [
            'projects' => $this->getDoctrine()->getRepository(Project::class)->findAll(),
        ]);
    }
    
    /**
     * @Route("/project/{id}", name="project")
     */
    public function projectAction(Project $project)
    {
        return $this->render('project/project.html.twig', [
            'project' => $project,
        ]);
    }

    /**
     * @Route("/project/{id}/edit", name="edit_project")
     */
    public function editProjectAction(Project $project, Request $request)
    {
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('projects');
        }

        return $this->render('project/project_form.html.twig', array(
            'form' => $form->createView(),
            'project' => $project,
        ));
    }

    /**
     * @Route("/add-project", name="add_project")
     */
    public function addProjectAction(Request $request)
    {
        $project = new Project();
        $form = $this->createForm(ProjectType::class, $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('projects');
        }

        return $this->render('project/project_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
