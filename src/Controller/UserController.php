<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use App\Entity\User;
use App\Form\UserType;

class UserController extends Controller
{
    /**
     * @Route("/users", name="users")
     */
    public function usersAction()
    {
        return $this->render('user/users.html.twig', [
            'users' => $this->getDoctrine()->getRepository(User::class)->findAll(),
        ]);
    }

    /**
     * @Route("/user/{id}", name="user")
     */
    public function userAction(User $user)
    {
        return $this->render('user/user.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/add-user", name="add_user")
     */
    public function addUserAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('users');
        }

        return $this->render('user/user_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
