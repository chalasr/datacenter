<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\File;
use App\Form\FileType;

class FileController extends Controller
{
    /**
     * @Route("/files", name="files")
     */
    public function filesAction()
    {
        return $this->render('file/files.html.twig', [
            'files' => $this->getDoctrine()->getRepository(File::class)->findAll(),
        ]);
    }

    /**
     * @Route("/add-file", name="add_file")
     */
    public function addFileAction(Request $request)
    {
        $file = new File();
        $form = $this->createForm(FileType::class, $file);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $file->setCreatedAt(new \DateTime());
            $em->persist($file);
            $em->flush();

            return $this->redirectToRoute('files');
        }

        return $this->render('file/file_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/game/{id}/delete", name="delete_file")
     */
    public function deleteFileAction(File $file)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($file);
        $em->flush();

        return $this->redirectToRoute('files');
    }
}
