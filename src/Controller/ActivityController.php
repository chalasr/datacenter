<?php

namespace App\Controller;

use App\Repository\ActivityRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Activity;
use App\Form\ActivityType;
use App\Entity\UserHasScore;
use App\Form\UserHasScoreType;

class ActivityController extends Controller
{
    /**
     * @Route("/activities", name="activities")
     * @Template("activity/activities.html.twig")
     * @param ActivityRepository $activityRepository
     * @return array
     */
    public function activitiesAction(ActivityRepository $activityRepository)
    {
        return [
            'activities' => $activityRepository->findAllOrderByStart('DESC'),
        ];
    }

    /**
     * @Route("/activity/{id}", name="activity")
     * @Template("activity/activity.html.twig")
     * @param Activity $activity
     * @return array
     */
    public function activityAction(Activity $activity)
    {
        return [
            'activity' => $activity,
        ];
    }

    /**
     * @Route("/activity/{id}/edit", name="edit_activity")
     * @Template("activity/activity_form.html.twig")
     * @param Activity $activity
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Activity $activity, Request $request)
    {
        $form = $this->createForm(ActivityType::class, $activity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($activity);
            $em->flush();

            return $this->redirectToRoute('add_user_score');
        }

        return [
            'form' => $form->createView(),
            'activity' => $activity,
        ];
    }

    /**
     * @Route("/add-user-score", name="add_user_score")
     * @Template("activity/user_score_form.html.twig")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addUserScoreAction(Request $request)
    {
        $userHasScore = new UserHasScore();
        $form = $this->createForm(UserHasScoreType::class, $userHasScore);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userHasScore);
            $em->flush();

            return $this->redirectToRoute('activities');
        }

        return [
            'form' => $form->createView()
        ];
    }
}
