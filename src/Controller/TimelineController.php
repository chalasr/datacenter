<?php

namespace App\Controller;

use App\Form\FromToType;
use App\Repository\ActivityRepository;
use App\Repository\TimelineRepository;
use App\TimelineManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TimelineController extends Controller
{
    /**
     * @Route("/timeline", name="timeline")
     * @Template("timeline/timeline.html.twig")
     * @param ActivityRepository $activityRepository
     * @param TimelineRepository $timelineRepository
     * @return array
     */
    public function timelineAction(TimelineManager $timelineManager, Request $request)
    {
        $from = new \DateTime('last month');
        $to = new \DateTime('now');
        $form = $this->createForm(FromToType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $from = $data['from'];
            $to = $data['to'];
        }

        return [
            'combinedTimeline' => $timelineManager->getCombinedTimeline($from, $to),
            'form' => $form->createView(),
        ];
    }
}