<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Data;
use App\Entity\Type;
use App\Entity\Link;
use App\Form\DataType;
use App\Form\LinkType;
use App\Form\TypeType;

class DataController extends Controller
{
    /**
     * @Route("/datas/{slug}", name="datas_type")
     * @Template("data/datas.html.twig")
     * @param Type $type
     * @return array
     */
    public function datasTypeAction(Type $type)
    {
        return [
            'datas' => $this->getDoctrine()->getRepository(Data::class)->findAllFromType($type),
            'types' => $this->getDoctrine()->getRepository(Type::class)->findAll(),
        ];
    }

    /**
     * @Route("/datas", name="datas")
     * @Template("data/datas.html.twig")
     * @return array
     */
    public function datasAction()
    {
        return [
            'datas' => $this->getDoctrine()->getRepository(Data::class)->findAll(),
            'types' => $this->getDoctrine()->getRepository(Type::class)->findAll(),
        ];
    }

    /**
     * @Route("/data/{id}", name="data")
     * @Template("data/data.html.twig")
     * @param Data $data
     * @return array
     */
    public function dataAction(Data $data)
    {
        return [
            'data' => $data,
        ];
    }

    /**
     * @Route("/data/{id}/edit", name="edit_data")
     * @Template("data/data_form.html.twig")
     * @param Data $data
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editDataAction(Data $data, Request $request)
    {
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('datas');
        }

        return [
            'form' => $form->createView(),
            'data' => $data
        ];
    }

    /**
     * @Route("/add-data", name="add_data")
     */
    public function addDataAction(Request $request)
    {
        $data = new Data();
        $form = $this->createForm(DataType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($data);
            $em->flush();

            return $this->redirectToRoute('datas');
        }

        return $this->render('data/data_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/ajax/links", name="ajax_links")
     */
    public function ajaxLinksAction(Request $request)
    {
        $links = null;
        foreach ($this->getDoctrine()->getRepository(Link::class)->getLinksFromUrl($request->get('q')) as $link) {
            $links[] = [
                'id' => $link->getId(),
                'text' => $link->getUrl()
            ];
        }

        return $this->json($links);
    }

    /**
     * @Route("/add-link", name="add_link")
     */
    public function addLinkAction(Request $request)
    {
        $link = new Link();
        $form = $this->createForm(LinkType::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();

            return $this->redirectToRoute('datas');
        }

        return $this->render('data/link_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/add-type", name="add_type")
     */
    public function addTypeAction(Request $request)
    {
        $type = new Type();
        $form = $this->createForm(TypeType::class, $type);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($type);
            $em->flush();

            return $this->redirectToRoute('datas');
        }

        return $this->render('data/type_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
