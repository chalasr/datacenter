<?php

namespace App\Controller;

use App\Entity\Type;
use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Game;
use App\Entity\Media;
use App\Form\MediaType;
use App\Form\GameType;

class GameController extends Controller
{
    /**
     * @Route("/games", name="games")
     */
    public function gamesAction()
    {
        return $this->render('game/games.html.twig', [
            'games' => $this->getDoctrine()->getRepository(Game::class)->findAll(),
        ]);
    }

    /**
     * @Route("/game/{id}", name="game")
     */
    public function gameAction(Game $game)
    {
        return $this->render('game/game.html.twig', [
            'game' => $game,
        ]);
    }

    /**
     * @Route("/game/{id}/md", name="game_md")
     */
    public function gameMdAction(Game $game)
    {
        return $this->render('game/game.md.twig', [
            'game' => $game,
            'host' => $this->getParameter('host') . 'uploads/',
        ]);
    }

    /**
     * @Route("/add-media", name="add_media")
     */
    public function addMediaAction(Request $request)
    {
        $media = new Media();
        $form = $this->createForm(MediaType::class, $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();

            return $this->redirectToRoute('games');
        }

        return $this->render('game/media_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/media/{id}/edit", name="edit_media")
     */
    public function editMediaAction(Media $media, Request $request)
    {
        $form = $this->createForm(MediaType::class, $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($media);
            $em->flush();

            return $this->redirectToRoute('games');
        }

        return $this->render('game/media_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/add-game", name="add_game")
     */
    public function addGameAction(Request $request)
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('games');
        }

        return $this->render('game/game_form.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/game/{id}/edit", name="edit_game")
     */
    public function editGameAction(Game $game, Request $request)
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($game);
            $em->flush();

            return $this->redirectToRoute('games');
        }

        return $this->render('game/game_form.html.twig', array(
            'form' => $form->createView(),
            'game' => $game
        ));
    }

    /**
     * @Route("/ajax/games", name="ajax_games")
     */
    public function ajaxGamesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $games = null;
        foreach ($em->getRepository(Game::class)->getGamesFromName($request->get('q')) as $game) {
            $games[] = [
                'id' => $game->getId(),
                'text' => $game->getName()
            ];
        }

        return $this->json($games);
    }

    /**
     * @Route("/ajax/medias", name="ajax_medias")
     */
    public function ajaxMediasAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $medias = null;
        foreach ($em->getRepository(Media::class)->getMediasFromName($request->get('q')) as $media) {
            $medias[] = [
                'id' => $media->getId(),
                'text' => $media->getName()
            ];
        }

        return $this->json($medias);
    }

    /**
     * @Route("/ajax/users", name="ajax_users")
     */
    public function ajaxUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = null;
        foreach ($em->getRepository(User::class)->getUsersFromUsername($request->get('q')) as $user) {
            $users[] = [
                'id' => $user->getId(),
                'text' => $user->getUsername()
            ];
        }

        return $this->json($users);
    }

    /**
     * @Route("/ajax/types", name="ajax_types")
     */
    public function ajaxTypesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $types = null;
        foreach ($em->getRepository(Type::class)->getTypesFromSlug($request->get('q')) as $type) {
            $types[] = [
                'id' => $type->getId(),
                'text' => $type->getSlug()
            ];
        }

        return $this->json($types);
    }
}
