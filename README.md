Datacenter
=====================
Powered by [Symfony](https://symfony.com/) and [Docker](https://www.docker.com/). Based on this [repository](https://github.com/maxpou/docker-symfony). 

Build
-----

### Dev

    make start
    make stop

The website is accessible at [localhost](http://localhost/app_dev.php/).

### Deploy

    git add -A
    git commit -m "v..."
    make deploy

The website is accessible at [datacenter.anthony-dessalles.com](http://datacenter.anthony-dessalles.com).